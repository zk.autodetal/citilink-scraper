package main

import (
	"bytes"
	"encoding/json"

	"github.com/boltdb/bolt"
)

type Specification struct {
	Id        string
	Brand     string
	Code      string
	Provider  string
	Source    string
	Timestamp string
	PropSet   map[string]map[string]string
	Images    []string
}

func JsonEncode(obj interface{}) ([]byte, error) {
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(obj)
	return buffer.Bytes(), err
}

var DB *bolt.DB

// данные хранятся бренд:артикул -> спецификация

func init() {
	var err error
	DB, err = bolt.Open("../products.db", 0600, nil)
	assert(err)
	err = DB.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte("citilink"))
		return nil
	})
	assert(err)
}

func save_specification(key string, spec *Specification) error {
	err := DB.Update(func(tx *bolt.Tx) error {
		ct_bkt := tx.Bucket([]byte("citilink"))
		json, err := JsonEncode(spec)
		if err == nil {
			return ct_bkt.Put([]byte(key), json)
		}
		return err
	})
	return err
}

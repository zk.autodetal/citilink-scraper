package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/parnurzeal/gorequest"
)

func NewSpecification(timestamp string) *Specification {
	s := new(Specification)
	s.Timestamp = timestamp
	s.PropSet = make(map[string]map[string]string)
	return s
}

func (s *Specification) Add(set_name, prop_name, prop_value string) {
	if _, ok := s.PropSet[set_name]; !ok {
		s.PropSet[set_name] = make(map[string]string)
	}
	s.PropSet[set_name][prop_name] = prop_value
}

func fetch(url string) (*goquery.Document, error) {
	client := new(http.Client)
	req, err := http.NewRequest("GET", url, nil)
	assert(err)
	req.Header.Set("User-Agent", user_agent)
	resp, err := client.Do(req)
	assert(err)
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	assert(err)

	return doc, err
}

func query_specs(doc *goquery.Document) *Specification {
	spec := NewSpecification(time.Now().Format(time.UnixDate))
	current_set := ""

	// Запрос спецификации
	//
	doc.Find(".SpecificationsFull").Each(func(n int, s *goquery.Selection) {
		s.Find(".Specifications__row, .SpecificationsFull__title").Each(func(n int, row *goquery.Selection) {
			if row.HasClass("SpecificationsFull__title") {
				current_set = strings.TrimSpace(row.Text())
				//fmt.Printf("\n::: %s\n", strings.TrimSpace(row.Text()))
			} else {
				name := row.Find(".Specifications__column_name").Contents().Not(".Tooltip")
				name_txt := strings.TrimSpace(name.First().Text())
				value_txt := strings.TrimSpace(row.Find(".Specifications__column_value").Text())
				spec.Add(current_set, name_txt, value_txt)
			}
		})
	})

	// Запрос картинок
	//
	doc.Find(".PreviewList__image").Each(func(n int, s *goquery.Selection) {
		src, has := s.First().Attr("src")
		if has {
			spec.Images = append(spec.Images, src)
		}
	})

	if len(spec.PropSet) == 0 {
		return nil
	} else {
		return spec
	}
}

func parse_spec_page(url string) *Specification {
	request := gorequest.New().AppendHeader("User-Agent", user_agent)
	//request_proxy := request.Proxy("http://201.55.32.229:3128")
	requests := []*gorequest.SuperAgent{request}

	var spec *Specification

	for _, rq := range requests {
		for n := 0; n < 5; n++ {
			_, body, errs := rq.Get(url).End()
			if len(errs) > 0 {
				for _, err := range errs {
					fmt.Println(err)
				}
				panic(nil)
			} else {
				doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
				assert(err)
				spec = query_specs(doc)
				if spec != nil {
					break
				}
			}
			fmt.Printf("properties request attempt %d \n", n)
		}
		if spec != nil {
			break
		}
		fmt.Println("switch proxy")
	}
	return spec
}

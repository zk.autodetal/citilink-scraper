package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/parnurzeal/gorequest"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"tideland.dev/go/slices"
)

const user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"

func Map[K comparable, V any](keys []K, vals []V) map[K]V {
	var result = make(map[K]V)
	if len(keys) == len(vals) && len(vals) > 0 {
		for n, k := range keys {
			result[k] = vals[n]
		}
	}
	return result
}

func assert(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
}

func normalize_code(code string) string {
	rx := regexp.MustCompile(`[^\wа-яА-Я]|[_]`)
	return strings.ToUpper(rx.ReplaceAllString(code, ""))
}

func normalize_link(code string) string {
	p := strings.Split(code, "/")
	return fmt.Sprintf("https://www.%s/%s/%s/", p[2], p[3], p[4])
}

func check_row(row []string) bool {
	test := slices.Map(func(s string) bool { return len(s) > 0 }, row)
	return slices.ContainsAll(func(v bool) bool { return v }, test)
}

func proxy_test(page_url string) {
	// px_url, err := url.Parse("https://167.172.104.11:40269")
	// fmt.Println(px_url)
	// assert(err)
	// client := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(px_url)}}
	// resp, err := client.Get(page_url)
	// assert(err)
	// txt, err := ioutil.ReadAll(resp.Body)
	// assert(err)
	// fmt.Println(txt)
	request := gorequest.New()
	_, body, errs := request.Proxy("http://201.55.32.229:3128").Get(page_url).End()
	if len(errs) != 0 {
		for _, err := range errs {
			fmt.Println(err)
		}
		panic(nil)
	} else {

		fmt.Println(body)
	}
}

func first_upper(s string) string {
	return cases.Title(language.Und, cases.NoLower).String(s)
}

func get_id(url string) string {
	rx := regexp.MustCompile(`(?P<ID>\d*)(\/properties\/)$`)
	return Map(rx.SubexpNames(), rx.FindStringSubmatch(url))["ID"]
}

func get_long_link(short_link string) string {
	req, err := http.NewRequest("GET", normalize_link(short_link), nil)
	assert(err)
	req.Header.Set("User-Agent", user_agent)

	client := new(http.Client)

	resp, err := client.Do(req)
	assert(err)
	return resp.Request.URL.String()

}

// proxy list
// http://201.55.32.229:3128
//

func main() {

	// В файле excel должны содержаться только колонки БРЕНД | АРТИКУЛ | КОРОТКАЯССЫЛКА
	//
	file_sl, err := os.Open("short-links.csv")
	assert(err)
	defer file_sl.Close()

	reader := csv.NewReader(file_sl)
	reader.Comma = '\t'
	reader.FieldsPerRecord = 3

	rows, err := reader.ReadAll()
	assert(err)

	var start int
	var num int
	fmt.Sscanf(os.Args[1], "%d:%d", &start, &num)
	fmt.Printf("парсинг %d ссылок, начиная с %d\n", num, start)

	for n, row := range rows[start:(start + num)] {
		if check_row(row) {
			brand := normalize_code(row[0])
			code := normalize_code(row[1])
			short_link := normalize_link(row[2])
			request := gorequest.New().AppendHeader("User-Agent", user_agent)

			var prop_link string
			for n := 1; n < 7; n++ {
				resp, _, errs := request.Get(short_link).End()
				if len(errs) > 0 {
					for _, err := range errs {
						fmt.Println(err)
					}
					panic(nil)
				}
				prop_link = fmt.Sprintf("%sproperties/", resp.Request.URL.String())
				if get_id(prop_link) != "" {
					break
				}
				fmt.Printf("long link request attempt %d \n", n)
			}

			spec := parse_spec_page(prop_link)

			if spec != nil {
				spec.Id = get_id(prop_link)
				if spec.Id != "" {
					spec.Provider = "citilink"
					spec.Source = prop_link
					spec.Brand = first_upper(brand)
					spec.Code = code

					assert(save_specification(fmt.Sprintf("%s:%s", brand, code), spec))
					fmt.Printf("%d / %s / %s / %s \n", start+n, spec.Id, spec.Code, spec.Brand)
				}
			} else {
				fmt.Println("REQUEST REFUSED")
				os.Exit(0)
			}
		}
	}
}
